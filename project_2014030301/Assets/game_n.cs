﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class game_n : MonoBehaviour {
	
	public GameObject[] cubes;  //立方体的种类
	public GameObject gameBg;  //时间用完之后显示的界面
	public int rows = 6;  //标记默认行数
	public int columns = 6; //标记默认列数 ,先目前只限于n*n型的
	public int CubeStyle_Size;//立方体的种类数
	public float duration = 2f;
	public float gameTime = 100f;   //游戏的总时间
	private GameObject particle = null;   //粒子效果
	private GameObject[,] cube_arr = null;  //标记每一个立方体,便于查找
	private bool gameOver = false;      
	public int click = 0;
	public GameObject cub1;  //在记录第一个点击的cub时对其进行保存遇到一定的问题，所以就定义为全局变量
	
	/// <summary>
	/// 1.创建立方体墙面，然后检查时候有可消除的
	/// </summary>
	
	void Start () {
		cube_arr = new GameObject[rows,columns];  
		CubeStyle_Size = cubes.Length;
		BuildWall ();
		Crash ();
	}
	
	
	void Update(){
		
		if (gameTime >= 0)
		{
			gameTime -= Time.deltaTime;
		}
	}
	
	/**
	 * 实时记录时间
   */
	void OnGUI(){		
		GUILayout.Label ("GameTime： " +gameTime.ToString("F0"));  //只显示整数位
		if (gameTime < 0) 
		{
			NGUITools.SetActive(gameBg, true);
		}	
	}
	
	/// <summary>
	/// 创建 n * n的立方体墙 
	/// </summary>
	void BuildWall(){		
		for(int i = 0; i < rows; i ++)
		{
			for(int j = 0; j < columns; j ++)
			{
				int r = Random.Range (0,CubeStyle_Size);
				GameObject cub = Instantiate(cubes[r],new Vector3(i,j,0), Quaternion.identity) as GameObject;
				//	HitCube hc = GameObject.Find("CubeOnMouseDown").GetComponent<HitCube>();
				cub.AddComponent<HitCube>();
				cube_arr[i,j] = cub;
			}
		}	
	}
	
	
	/// <summary>
	/// 检测到立方体被点击之后存放点击前后两个立方体及其位置
	/// </summary>
	
	public void move(GameObject cub1, GameObject cub2){		
		Vector3 pos1 = cub1.transform.position;
		Vector3 pos2 = cub2.transform.position;
		int colSames=0;
		int rowSames = 0;
		int i = 0;
		int j = 0;
		bool isChecked = false;   //检查是否是上下左右方向
		//Debug.Log ("distance" + Vector3.Distance (pos1, pos2));
		if(Vector3.Distance (pos1, pos2) == 1)   //判断是否是周围方块
		{
			//将两次点击的物体进行交换
			GameObject cub1_temp = cube_arr[(int)pos1.x, (int)pos1.y];
			GameObject cub2_temp = cube_arr[(int)pos2.x, (int)pos2.y];		
			cube_arr[(int)pos1.x, (int)pos1.y] = cub2_temp;  
			cube_arr[(int)pos2.x, (int)pos2.y] = cub1_temp;
			//	Debug.Log("cub1" + cub1.transform.position + "pos2 " + pos2);			
			isChecked = CheckData (out rowSames, out colSames, out i, out j);			
			//	Debug.Log("ischecked" + isChecked);
			if(isChecked == true)
			{
				iTween.MoveTo(cub1, iTween.Hash("position", pos2, "time", duration));
				//	iTween.MoveTo(cub2, iTween.Hash("position", pos1, "time", duration, "oncomplete", "StartCrash", "oncompletetarget", gameObject));
				iTween.MoveTo(cub2, iTween.Hash("position", pos1, "time", duration));
				Invoke("Crash", 1f);  //是每次Crash的时候产生一定的时间间隔 1秒之后再调用
			}else
			{
				cube_arr[(int)pos1.x, (int)pos1.y] = cub1_temp;  
				cube_arr[(int)pos2.x, (int)pos2.y] = cub2_temp;
			}
		}
	}
	
	/// <summary>
	/// 检查时候满足移动之后有超过三个以上的相同的立方体在一排或一列，如果满足则实现移动，要是不满足则不移动
	/// </summary>
	
	bool CheckData(out int rowSames, out int colSames, out int x, out int y){
		rowSames = 0;
		colSames = 1;
		x = 0;
		y = 0;	

		//check (rows, columns); //检查某行上的某列的相同数量
		//check (columns, rows); //检查某列上的某行的相同数量
		for (int i = 0; i < rows; i ++)
		{
			for (int j = 0; j < columns; j ++) 
			{
				//以某行或某列的第一个为参照物
				if (j == 0)
				{
					rowSames = 1;
					colSames = 1;
				} else 
				{
					if (cube_arr [i, j - 1].name == cube_arr [i, j].name)
						rowSames++;  //i列的相同cube的数量
					else
					{
						if(rowSames >= 3)
						{
							x = i;
							y = j - 1;
							return true;
						}
						rowSames = 1;
					}
					if (cube_arr [j - 1, i].name == cube_arr [j, i].name)
						colSames ++;
					else
					{
						if (colSames >= 3) 
						{						
							x = i;
							y = j - 1;
							//Debug.Log ("rowSames = " + rowSames + "colSame = " + colSames + "i = " + x + " j = " + y);
							return true;
						}						
						colSames = 1;
					}
					if ((j == rows - 1 && rowSames >= 3) || (j == columns - 1 && colSames >= 3)) 
					{
						x = i;
						y = j;
						return true;
					}
				}
			}
		}
		return false;
		
	}

	//检查
	void check(int m, int n){
		for (int i = 0; i < m; i ++) 
		{

		}
	}
	/// <summary>
	/// 进行消除操作
	/// </summary>
	void Crash()
	{
		int rowSames = 0;
		int colSames = 0;
		int i = 0;
		int j = 0;		
		CheckData(out rowSames, out colSames, out i, out j);
		// Debug.Log ("rowSames = " + rowSames + "colSame = " + colSames + "i = " + i + " j = " + j);
		//第i列的相同cube进行消除
		if (rowSames >= 3) 
		{
			int k = 0;
			List<GameObject> cube_list = new List<GameObject> ();
			for (int index = 1; index <= rowSames; index++) {
				int r = Random.Range (0, CubeStyle_Size);
				//在立方体下面实例化立方体代替即将消失的立方体
				//cube的y向上是加
				GameObject obj = Instantiate (cubes [r], new Vector3 (i, rows + index - 1, 0), Quaternion.identity) as GameObject;//在顶部实例化一个新cube
				//HitCube hc = GameObject.Find("CubeOnMouseDown").GetComponent<HitCube>();
				obj.AddComponent<HitCube>();
				cube_list.Add (obj);
				//在消除之前将该cube形成一个从大到无的效果创建一个消除一个
				//	iTween.ScaleTo(cube_arr [i, j - rowSames + index], iTween.Hash("scale", new Vector3(0, 0.01f, 0), "time", duration));		
				Destroy (cube_arr [i, j - rowSames + index]);
			}
			for(int n = 1; n < rows - j + rowSames; n ++)
			{
				if ((j+n>= rows)) 
				{
					cube_arr [i, j - rowSames + n] = cube_list [k];
					k ++;
				} else  
				{
					cube_arr [i, j - rowSames + n] = cube_arr [i, j + n];
				}
				
				//	iTween.MoveTo (cube_arr [i, j - rowSames + n], iTween.Hash ("position", new Vector3 (i, j - rowSames + n, 0), "time", duration, "oncomplete", "StartCrash", "oncompletetarget", gameObject));
				iTween.MoveTo (cube_arr [i, j - rowSames + n], iTween.Hash ("position", new Vector3 (i, j - rowSames + n, 0), "time", duration));
				Invoke("Crash",1f); 
			}
		}
		
		//某一行有相同可消除的			
		else if(colSames >= 3)
		{
			int k = 0; 
			List<GameObject> cube_list = new List<GameObject>();
			for(int index = 1; index <= colSames; index ++)
			{
				int r = Random.Range(0, CubeStyle_Size);
				GameObject obj = Instantiate(cubes[r], new Vector3(j-colSames+index, rows,0),Quaternion.identity) as GameObject;
				obj.AddComponent<HitCube>();
				//HitCube hc = GameObject.Find("CubeOnMouseDown").GetComponent<HitCube>();
				cube_list.Add(obj);
				//	iTween.ScaleTo(cube_arr [j - colSames + index,i], iTween.Hash("scale", new Vector3(0, 0.01f, 0), "time", duration));
				Destroy(cube_arr[j-colSames+index, i]);		
				//在消除的该位置产生粒子效果
				//	Instantiate(part, new Vector3(i, j - colSames+index, -2),Quaternion.identity); 
				//将要消除的以上的所有列向下移动
				for(int n = 0; n < rows - i; n ++)
				{
					//已存在的向下
					if(i+n < rows - 1)
					{
						cube_arr[j-colSames+index,i+n] = cube_arr[j-colSames + index,i+n+1];
					}else
					{
						cube_arr[j-colSames+index,i+n] = cube_list[k];
						k ++;
					}
					//	iTween.MoveTo(cube_arr[j-colSames + index,i+n], iTween.Hash("position", new Vector3(j-colSames + index,i+n, 0), "time", duration,"oncomplete","StartCrash","oncompletetarget",gameObject));
					iTween.MoveTo(cube_arr[j-colSames + index,i+n], iTween.Hash("position", new Vector3(j-colSames + index,i+n, 0), "time", duration));
					Invoke("Crash",1f);
				}
			}
		}
		//yield return new WaitForSeconds(2);
	}
	
	
}
