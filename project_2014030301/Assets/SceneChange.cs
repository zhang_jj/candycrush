﻿using UnityEngine;
using System.Collections;

public class SceneChange : MonoBehaviour {

	public bool loadToGame = false;
	public bool back = false;

	
	void OnClick(){
		if (loadToGame) {
			Application.LoadLevel ("game01");
		}else if (back) {
			Application.LoadLevel("welcome");
		}
	}
}
