﻿using UnityEngine;
using System.Collections;

//[AddComponentMenu("Scripts/hitCube")]
 public class HitCube : MonoBehaviour {

	/// <summary>
	///用于cube被点击检测并记录下来 
	/// </summary>

	void OnMouseDown(){
		game_2 g = GameObject.Find ("Main Camera").GetComponent<game_2> ();
		if (g.gameTime >= 0) 
		{
			g.click ++;
			if (g.click == 1) {
					g.cub1 = this.gameObject;
			}
			if (g.click == 2) {
				g.move(g.cub1, this.gameObject);
				g.click = 0;
			}
		}
	}

}
